#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define COULEURS 6
#define TAILLE 12

SDL_Renderer *renderer;
SDL_Texture  *avatar;
SDL_Texture  *texttext;

SDL_Rect rect;
int iW, iH;
const int HEIGHT=750;
const int WIDTH=900;
int running=1;
int height=HEIGHT;
int width=WIDTH;
int aff=0;

void afficherEcran(void)
{
   // le renderer est créé
    SDL_Rect rect;
    int running=1;
    /* couleur de fond */
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
   	SDL_RenderClear(renderer);

   	/* dessiner en blanc */
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    rect.x = width*0.05;
    rect.y = height*0.05;
    rect.w = width/2;
    rect.h = height*0.90;
    SDL_RenderFillRect(renderer, &rect );

    int i;
    int j;
    int h;

    for (i=1;i<11;i++)
    {
        for (j=1;j<11;j++)
        {
            h=i*25;
            SDL_SetRenderDrawColor(renderer,i*j,i*j,h*j,255);
            rect.x = width*0.05*i;
            rect.y = height*0.05*j;
            rect.w = width*0.05;
            rect.h = height*0.90/12;
            SDL_RenderFillRect(renderer,&rect);
        }
    }

	/* afficher à l'ecran */
    SDL_RenderPresent(renderer);

    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    rect.x = 100;
    rect.y = 100;
    rect.w = 200;
    rect.h = 200;
    SDL_RenderFillRect(renderer,&rect);

    SDL_RenderPresent(renderer);
}


void initialiser(char T[TAILLE][TAILLE])
{
    int i,j;
    for (i=0;i<TAILLE;i++)
    {
        for (j=0;j<TAILLE;j++)
        {
            T[i][j]=rand()%COULEURS;
        }
    }
}

void afficherGrille(char T[TAILLE][TAILLE])
{
    int i,j;
    for (i=0;i<TAILLE;i++)
    {
        for (j=0;j<TAILLE;j++)
        {
            printf("%d  ",T[i][j]);
        }
        printf("\n");
    }
}

int fin(char T[TAILLE][TAILLE])
{
    int c=1;
    int i,j;
    for (i=0;i<TAILLE;i++)
    {
        for (j=0;j<TAILLE;j++)
        {
            if (T[i][j]!=T[0][0])
            {
                c=0;
            }
        }
    }
    return c;
}

void remplir (char T[TAILLE][TAILLE] ,int av ,int ap,int i, int j)
{
    T[i][j]=ap;
    if (i-1>=0)
    {
        if (T[i-1][j]==av)
        {
            remplir(T,av,ap,i-1,j);
        }
    }
    if (j-1>=0)
    {
        if (T[i][j-1]==av)
        {
            remplir(T,av,ap,i,j-1);
        }
    }
    if (i+1<12)
    {
        if (T[i+1][j]==av)
        {
            remplir(T,av,ap,i+1,j);
        }
    }
    if (j+1<12)
    {
        if (T[i][j+1]==av)
        {
            remplir(T,av,ap,i,j+1);
        }
    }
}


int main ()
{
    char P[TAILLE][TAILLE];
    srand(time(NULL));

    initialiser(P);
    afficherGrille(P);

    int nb=23;
    int it=0;
    int coul=10;
    int i=0;
    int j=0;

    SDL_Event event;
    SDL_Window   * window;
    SDL_Rect rect;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    window = SDL_CreateWindow("SDL2 Programme 0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE); 
    if (window == 0) 
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) 
    {
       fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

/*
    if (TTF_Init() != 0)
    {
        fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 
    }
    
    TTF_Font * font1;
    font1 = TTF_OpenFont("chlorinar.regular.ttf", 72 );

    SDL_Color     couleur  = {0, 0, 255, 255};        
    SDL_Surface * surf     = TTF_RenderText_Blended(font1, "FloodIt", couleur);
    texttext = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_QueryTexture(texttext, NULL, NULL, &iW, &iH);
*/

/*
    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }
*/

    while (running) 
    {
        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:  
                            printf("appui sur la croix\n");	
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
    						printf("Size : %d %d\n", width, height);
    					default:
    						afficherEcran();
    				}   
    			    break;
    			case SDL_MOUSEBUTTONDOWN:
    				printf("Appui :%d %d\n", event.button.x, event.button.y);
    				/*if (event.button.x>200)
                    {
                        if (event.button.x<250)
                        {
                            if (event.button.y>30)
                            {
                                if (event.button.y<80)
                                {
                                  printf("c'est rouge\n");
                                    aff=1;
                                    afficherEcran();
                                }  
                            }
                        }
                    }
    				break;
                    */
                case SDL_MOUSEBUTTONUP:
                    printf("dd\n");
                    aff=0;
                    afficherEcran();
                    break;
    			case SDL_QUIT : 
    				printf("on quitte\n");    
    				running = 0;
    		}
    	}	
	    SDL_Delay(1); //  delai minimal
    }



/*
    while (it<nb && fin(P)!=1 && coul!=999)
    {
        printf("Choisissez une couleur : \n");
        scanf("%d",&coul);
        if (coul!=P[0][0])
        {
            remplir(P,P[0][0],coul,i,j);
            i=i+1;
            if (i==12);
            {
                i=0;
                j=j+1;
            }
            it=it+1;
        }
        afficherGrille(P);
        printf("\n");
        printf("Il te reste %d coup à jouer ! \n",nb-it);
    }
    printf("\n");
    if (it==nb)
    {
        printf("perdu... \n");
    }
    if (fin(P)==1)
    {
        printf("GAGNER ! n");
    }
*/
    
/*
    TTF_CloseFont(font1);
    TTF_Quit();
*/

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}