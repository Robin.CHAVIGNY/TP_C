#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hall_of_fame.h"

// un petit commentaire ?
void afficherDonnee(FILE * file, donnee_t d) 
{
	fprintf(file,"score : ");
	fprintf(file,"%d\n",d.score);
	fprintf(file,"nom du jeu : ");
	fprintf(file,"%s",d.nom);
	fprintf(file,"nom du joeur : ");
	fprintf(file,"%s",d.alias);
}

// un petit commentaire ?
void saisirDonnee(FILE *file, donnee_t * p)
{
	fprintf(file,"score ?\n");
	scanf("%d",&(p->score));
	fprintf(file,"nom du jeu ?\n");
	fgets(p->nom,SIZE_JEU,file);
	fprintf(file,"nom du joeur ?\n");
	fgets(p->alias,SIZE_ALIAS,file);
}

int tableauFromFilename(char *fichier,donnee_t *tab)
{
	int i=0;
	FILE *file;
	
	file=fopen(fichier,"r");
	if (file)
	{
		while (fgets(tab[i].nom,SIZE_JEU,file)!=NULL && i<TAILLE_MAX)
			fgets(tab[i].alias,SIZE_ALIAS,file);
			fscanf(file,"%d%*c",&tab[i].score);
			i++;
	}
	return i;
}

int main()
{
	return 0;
}
