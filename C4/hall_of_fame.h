#ifndef hall_of_fame_h
#define hall_of_fame_h

/* DECLARATION DES TYPES PERSONNELS */
// et de leur typedef si besoin
#define SIZE_JEU 100
#define SIZE_ALIAS 40
#define TAILLE_MAX 50

typedef struct donnee
{
	int score;
	char nom[SIZE_JEU];
	char alias[SIZE_ALIAS];
} donnee_t;

/* DECLARATIONS DES METHODES */
void afficherDonnee(FILE *, donnee_t);
void saisirDonnee (FILE * , donnee_t *);
int tableauFromFilename(char *fichier,donnee_t *);
// mettre ici les autres declarations

#endif
