#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

SDL_Renderer *renderer;
SDL_Texture  *avatar;
SDL_Texture  *texttext;

SDL_Rect rect;
int iW, iH;
const int HEIGHT=750;
const int WIDTH=900;
int running=1;
int height=HEIGHT;
int width=WIDTH;
int aff=0;

void afficherEcran(void)
{
   // le renderer est créé
    SDL_Rect rect;
    int running=1;
    /* couleur de fond */
	SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
   	SDL_RenderClear(renderer);

   	/* dessiner en blanc */
    SDL_SetRenderDrawColor(renderer, 125, 255, 125, 255);
    rect.x = rect.y = 100;
    rect.w = rect.h = 300;
    SDL_RenderFillRect(renderer, &rect );

    SDL_SetRenderDrawColor(renderer,255,0,0,255);
    rect.x=200,rect.y=30;
    rect.w=rect.h=50;
    SDL_RenderFillRect(renderer,&rect);

    

    rect.x = 400;
    rect.y = 200;
    rect.w = iW;
    rect.h = iH;
    SDL_RenderCopy(renderer, texttext, NULL, &rect);

    if (aff==1)
        {
            rect.x = width/2;
            rect.y = height/2;
            rect.w = 128*width/WIDTH;
            rect.h = 128*height/HEIGHT;
            SDL_RenderCopy(renderer, avatar, NULL, &rect); 
        }
	/* afficher à l'ecran */
    SDL_RenderPresent(renderer);
}

/*void dessinerEcran() 
{
	switch(mode) {
	   INTRODUCTION : afficherIntroduction();
	      break;
	   JEU : afficherEcranDeJeu();
	   	  break;
	   FIN : afficherFin();
	}
}

enum ETAT 
{
   INTRODUCTION, JEU, FIN
};
*/

int main ()
{
    SDL_Event event;
    SDL_Window   * window;
    SDL_Rect rect;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
     fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
     return EXIT_FAILURE; 
    }
    
    window = SDL_CreateWindow("SDL2 Programme 0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
            width, height, 
            SDL_WINDOW_RESIZABLE); 
    
    if (window == 0) 
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) {
       fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    if (TTF_Init() != 0)
    {
        fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 
    }
    
    TTF_Font * font1;
    font1 = TTF_OpenFont("chlorinar.regular.ttf", 72 ); 

    SDL_Color     couleur  = {0, 0, 255, 255};        
    SDL_Surface * surf     = TTF_RenderText_Blended(font1, "FloodIt", couleur);
    texttext = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_QueryTexture(texttext, NULL, NULL, &iW, &iH);


    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }

    SDL_Surface *image = NULL;
    image=IMG_Load("loic.png");
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }

    avatar = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);

    while (running) 
    {
        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    printf("window event\n");
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:  
                            printf("appui sur la croix\n");	
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
    						printf("Size : %d %d\n", width, height);
    					default:
    						afficherEcran();
    				}   
    			    break;
    			case SDL_MOUSEBUTTONDOWN:
    				printf("Appui :%d %d\n", event.button.x, event.button.y);
    				if (event.button.x>200)
                    {
                        if (event.button.x<250)
                        {
                            if (event.button.y>30)
                            {
                                if (event.button.y<80)
                                {
                                  printf("c'est rouge\n");
                                    aff=1;
                                    afficherEcran();
                                }  
                            }
                        }
                    }
    				break;
                case SDL_MOUSEBUTTONUP:
                    printf("dd");
                    aff=0;
                    afficherEcran();
                    break;
    			case SDL_QUIT : 
    				printf("on quitte\n");    
    				running = 0;
    		}
    	}	
	    SDL_Delay(1); //  delai minimal
    }
    
    TTF_CloseFont(font1);
    TTF_Quit();
    IMG_Quit();
    SDL_DestroyTexture(avatar);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    

    SDL_Quit();
    return 0;
}
