#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct cellule 
{
  char             ligne[255];
  struct cellule * suiv;
} cellule_t;

typedef struct liste 
{
  cellule_t * tete;
  cellule_t * fin;
} liste_t;

liste_t * liste_vide()
{
    liste_t * vide = (liste_t *) malloc (sizeof (liste_t));
    vide->tete=NULL;
    vide->fin=NULL;
    return vide;
}
void insertion_en_tete(liste_t * L, char s[])
{
    cellule_t * c = (cellule_t *) malloc (sizeof (cellule_t));
    strcpy(c->ligne,s);
    c->suiv=L->tete;
    if (L->tete == NULL)
    {
        L->fin=c->suiv;
    }
    L->tete=c;
}

void ajout_en_fin(liste_t * L,char s[])
{
    cellule_t * c =(cellule_t *) malloc (sizeof (cellule_t));
    strcpy(c->ligne,s);
    c->suiv=NULL;
    if (L->tete==NULL)
    {
        L->tete=c;
    }
    else
        L->fin->suiv=c;
    L->fin=c;
}

void afficher(liste_t * P)
{
    int i = 1;
    cellule_t * tmp= P->tete;
    while (tmp!=NULL)
    {
        printf("cellule %d:\n %s\n",i,tmp->ligne);
        tmp=tmp->suiv;
        i++;
    }
}

int main(int argc,char *argv[]) 
{
    liste_t * L= liste_vide();
    char b[255];
    char * fin="#f#\n";

    FILE *f;
    char c;
    
    strcpy(b,"");

    if (argc==1)
    {
        while (strcmp(b,fin))
        {
            puts("donnez une chaine: ");
            fgets(b,255,stdin);
            ajout_en_fin(L, b);
        }
    }
    else
    {
        f=fopen(argv[1],"r");
        while (strcmp(b,fin))
        {
            fgets(b,255,f);
            printf("%s\n",b);
            ajout_en_fin(L,b);
        }
    }
    afficher(L);
    return 0;
}