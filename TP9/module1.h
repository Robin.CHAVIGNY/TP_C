#ifndef _MODULE1_H_
#define _MODULE1_H_

typedef enum ope 
{
    NONE = -1, ID , SIN, COS, LOG, EXP
} OP;

int identification(char T[]);
int evalf(double d,int i);
#endif