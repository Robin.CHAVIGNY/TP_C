#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "teZZt.h"
#include "module1.h"

BEGIN_TEST_GROUP(tableau_structure)

TEST(identification_avec_indice) 
{
  CHECK(  0 == identification("x"));
  CHECK( -1 == identification("x\n"));
  CHECK(  1 == identification("sin(x)"));
  CHECK(  2 == identification("cos(x)"));
  CHECK(  3 == identification("log(x)"));
  CHECK(  4 == identification("exp(x)"));
  CHECK( -1 == identification("t"));
}

TEST(identification_avec_indice) {
  CHECK(  0 == identification("x"));
  CHECK( -1 == identification("x\n"));
  CHECK(  1 == identification("sin(x)"));
  CHECK(  2 == identification("cos(x)"));
  CHECK(  3 == identification("log(x)"));
  CHECK(  4 == identification("exp(x)"));
  CHECK( -1 == identification("t"));
}

END_TEST_GROUP(tableau_structure)


int main(void) {
	RUN_TEST_GROUP(tableau_structure); 
 	return 0;
}