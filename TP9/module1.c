#include <stdio.h>
#include <string.h>
#include <math.h>

const char * OPER_NAMES[] = { "x", "sin(x)", "cos(x)", "log(x)", "exp(x)", NULL };


int identification(char T[])
{
    int c = 0;
    while (OPER_NAMES[c]!=NULL && strcmp(T,OPER_NAMES[c]))
    {
        c++;
    }   
    if (c==5)
    {
        c=-1;
    }
    return (c);
}

int evalf(double d,int i)
{
    if (i==0)
    {
        return (d);
    }
    else if (i==1)
    {
        return (sin(d));
    }
    else if (i==2)
    {
        return (cos(d));
    }
    else if (i==3)
    {
        return (log(d));
    }
    else if (i==4)
    {
        return exp(d);
    }
}

int calcul()